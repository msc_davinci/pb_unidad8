package aspectodos;

public class Electrical extends EnergySource {
    private double powerOutput;

    public double getPowerOutput() {
        return powerOutput;
    }

    public void setPowerOutput(double powerOutput) {
        this.powerOutput = powerOutput;
    }
}
