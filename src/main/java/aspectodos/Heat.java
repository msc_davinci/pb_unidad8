package aspectodos;

public class Heat extends EnergySource {
    private double maxTemperature;

    public double getMaxTemperature() {
        return maxTemperature;
    }

    public void setMaxTemperature(double maxTemperature) {
        this.maxTemperature = maxTemperature;
    }
}
