package aspectodos;

public class Main {
    private static Heat heatSource;
    private static Electrical electricalSource;
    private static Mecanical mecanicalSource;


    public static void main(String[] args) {
        heatSource = new Heat();
        electricalSource = new Electrical();
        mecanicalSource = new Mecanical();

        //Print cost, volts, fuel used and max temperature for Heat Source
        heatSource.setFirstCost(125.34);
        heatSource.setVolts(220);
        heatSource.setFuelUsed("Anticongelante");
        heatSource.setMaxTemperature(34.6);
        System.out.printf("\nHeat Source: El costo es $%.2f, el voltaje que soporta es %.2f V, el combustible utilizado es %s y " +
                "su temperatura máxima es de %.2f°C", heatSource.getFirstCost(), heatSource.getVolts(), heatSource.getFuelUsed(),
                heatSource.getMaxTemperature());

        //Print cost, volts, fuel used and power output for Electrical Source
        electricalSource.setFirstCost(234.67);
        electricalSource.setVolts(440);
        electricalSource.setFuelUsed("Electricidad");
        electricalSource.setPowerOutput(110);
        System.out.printf("\nElectrical Source: El costo es $%.2f, el voltaje que soporta es %.2f V, el combustible utilizado es %s y " +
                        "su potencia de salida es de %.2f HP", electricalSource.getFirstCost(), electricalSource.getVolts(),
                electricalSource.getFuelUsed(), electricalSource.getPowerOutput());

        //Print cost, volts, fuel used and max revolutions per minute for Mecanical Source
        mecanicalSource.setFirstCost(125.34);
        mecanicalSource.setVolts(220);
        mecanicalSource.setFuelUsed("Gasolina");
        mecanicalSource.setMaxRevolutionsPerMinute(1200);
        System.out.printf("\nMecanical Source: El costo es $%.2f, el voltaje que soporta es %.2f V, el combustible utilizado es %s y " +
                        "sus revoluciones por minutos es de %.2f", mecanicalSource.getFirstCost(), mecanicalSource.getVolts(),
                mecanicalSource.getFuelUsed(), mecanicalSource.getMaxRevolutionsPerMinute());
    }
}
