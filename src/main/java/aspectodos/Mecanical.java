package aspectodos;

public class Mecanical extends EnergySource {
    private double maxRevolutionsPerMinute;

    public double getMaxRevolutionsPerMinute() {
        return maxRevolutionsPerMinute;
    }

    public void setMaxRevolutionsPerMinute(double maxRevolutionsPerMinute) {
        this.maxRevolutionsPerMinute = maxRevolutionsPerMinute;
    }
}
