package aspectodos;

public class EnergySource {
    private double firstCost;
    private double volts;
    private String fuelUsed;


    public double getFirstCost() {
        return firstCost;
    }

    public void setFirstCost(double firstCost) {
        this.firstCost = firstCost;
    }

    public double getVolts() {
        return volts;
    }

    public void setVolts(double volts) {
        this.volts = volts;
    }

    public String getFuelUsed() {
        return fuelUsed;
    }

    public void setFuelUsed(String fuelUsed) {
        this.fuelUsed = fuelUsed;
    }
}
